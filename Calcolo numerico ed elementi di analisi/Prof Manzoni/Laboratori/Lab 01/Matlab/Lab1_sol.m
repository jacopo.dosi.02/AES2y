%% Laboratorio 1 - Calcolo Numerico ed Elementi di Analisi - AER 

%% Esercizio 1
v1 = 2.^[0:10]
v2 = cos (pi ./ [1:10])'

format short g 
v3 = 0.1 .* 2 .^ [0:-1:-5] 

v4 = zeros (1, 19);
v4 (1:2:19) = exp ([1:10]) - (-1) .^ [1:10] .* (1 + 5 * [1:10])

%% Esercizio 2
A = diag (2 * ones (5,1)) + diag (5 * ones (1, 4), -1) ...
    + diag (10 * ones (1, 3), -2) + diag (10 * ones (1, 3), 2) ... 
    + diag ([40], 4) + diag ([40], -4)

sum (sum (A))
    
A (1:3, 1:3)

A ([1 3 5], [1 2 4])
   
A (2:4, [1 3 4])
    
%% Esercizio 3
B = diag (ones (1, 10)) + [0, ones(1,8), 0]' * [1, zeros(1,8), 1] ... 
    + [1, zeros(1,8), 1]' * [0, ones(1,8), 0];

C = diag ([1:200]) + diag (ones (1, 199), 1) + diag (ones (1, 199), -1) ... 
    + diag (0.5 * ones (1, 198), 2) + diag (0.5 * ones (1, 198), -2);

D = diag ([20:-2:2]) + diag (3.^[0:8], 1) + diag (0.5 * ones (8, 1), -2);

%% Esercizio 4
x=[0:3];
f1=@(x) x.*sin(x)+(1/2).^(sqrt(x));
f1(x)

f2=@(x) x.^4+log(x.^3+1);
f2(x)

%% Esercizio 5
x=[0:0.01:6];
f=@(x) 2+(x-3).*sin(5*(x-3)); 
r1=@(x) -x+5;
r2=@(x) x-1;
plot(x,f(x),'k');
hold on
plot(x,r1(x),'--k')
plot(x,r2(x),'--k')

%% Esercizio 6
x=[0.1:0.01:10];
logf=inline('(log(x)).^2','x'); 
semilogx(x,logf(x))
