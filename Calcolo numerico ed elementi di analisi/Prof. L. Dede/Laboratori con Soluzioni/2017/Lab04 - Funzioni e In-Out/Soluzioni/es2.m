clc
clear

n=10;
a=zeros(n,1);
for k=1:n
    if k==2 || k==6
        a(k)=1/k;
    else
        a(k)=1/((k-2)*(k-6));
    end
end
a
