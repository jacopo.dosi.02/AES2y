function T = es19(n)        
T = zeros(n);
T(1:2:n, 1:2:n) = 1;
T(2:2:n, 2:2:n) = 1;
end
