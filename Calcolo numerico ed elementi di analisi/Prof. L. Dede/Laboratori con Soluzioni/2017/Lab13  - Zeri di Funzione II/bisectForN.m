function [x_vect,k] = bisect(a,b,tol,itermax,fun)


x = (a+b)/2;

x_vect = [x];

k = 0;

if (fun (a) * fun (b) > 0)
    error ('La funzione deve avere segno diverso nei due estremi');
end

while (k < itermax) & (abs(fun(x))>tol)
    
    k = k+1;
    
    if fun(x) == 0
        return
    elseif fun(a)*fun(x) < 0
        b = x;
    else
        a = x;
    end
    
    x = (a+b)/2;

    x_vect = [x_vect; x];
    
end


 fprintf(' Radice calcolata : %.8f \n', x_vect(end))


end

