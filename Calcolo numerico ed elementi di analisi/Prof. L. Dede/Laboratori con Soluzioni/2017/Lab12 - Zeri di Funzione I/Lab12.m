
close all

%% 1.1

f = @(x) x.^3 - (2+exp(1)).*x.^2 + (2*exp(1) + 1).*x + (1-exp(1)) - cosh(x-1)
x = [0.5:0.001:6.5];
plot(x,f(x))
grid on

%% 1.2

disp('la prima radice appartiene all''intervallo [0.5, 1.5]');
if (f(0.5)*f(1.5)) < 0
       disp('->si puo'' applicare il metodo di bisezione alla prima radice ');
   else
       disp('->non si puo'' applicare il metodo di bisezione alla prima radice');
end
   
disp('la seconda radice appartiene all''intervallo [3, 4]');
if (f(3)*f(4)) < 0
       disp('->si puo'' applicare il metodo di bisezione alla seconda radice');
   else
       disp('->non si puo'' applicare il metodo di bisezione alla seconda radice');
end
   
disp('la terza radice appartiene all''intervallo [6, 6.5]');
if (f(6)*f(6.5)) < 0
       disp('->si puo'' applicare il metodo di bisezione alla terza radice');
   else
       disp('->non si puo'' applicare il metodo di bisezione alla terza radice');
end
   
a = 3;
b = 4;
tol = 1e-12;

[x_vect, k] = bisect(a,b,tol,f)



%% EX 2
% syms x
% f = f(x);
% diff(f,x)


x0 = 0.5;
der_f = @(x) 3*x.^2 - (2+exp(1))*2.*x + (2*exp(1) + 1) - sinh(x-1)
itermax = 1e+4;
tol = 1e-16;
m = 2;

[x_vect1,k1] = newton(x0,itermax,tol,f,der_f) %% Non dico nulla sulla derivata -> la function prende m = 1, cio? Newton non modificato)

[x_vect2,k2] = newton(x0,itermax,tol,f,der_f,m)


figure
semilogy([0:k1], abs(f(x_vect1)),'r');
hold on
semilogy([0:k2], abs(f(x_vect2)), 'b');
grid on
legend('Newton','Newton Mod.')






