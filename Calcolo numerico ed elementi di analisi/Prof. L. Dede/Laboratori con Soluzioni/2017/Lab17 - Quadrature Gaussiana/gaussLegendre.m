function I = gaussLegendre(f,a,b,n)

knots = [];
weights = [];

if n == 0
    knots = [0];
    weights = [2];
elseif n == 1
    knots = [-(3)^(-0.5) (3)^(-0.5)];
    weights = [1 1];
elseif n == 2
    knots = [-sqrt(15)/5 0 sqrt(15)/5];
    weights = [5/9 8/9 5/9];
else
   error('Accontentati di n più piccolo (<= 2), please') 
end

% Let's rescale knots and weights
knots = (a+b)/2 + ((b-a)/2).*knots;
weights = ((b-a)/2).*weights;


I = dot( weights, f(knots) );
    


end

