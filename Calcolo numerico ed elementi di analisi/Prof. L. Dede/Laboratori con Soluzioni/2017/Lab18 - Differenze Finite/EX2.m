f = @(x) exp(-x.^2).*sin(2*x+1);
a = -3; b = 3;
h = 0.1;
x = [a+h:h:b-h];
df = @(x) 2*exp(-x.^2).*cos(2*x + 1) - 2*x.*exp(-x.^2).*sin(2*x + 1);



cfd_vect = [];


for x_i = a+h:h:b-h
    

    cfd = (f(x_i + h) - f(x_i - h))/(2*h);

    cfd_vect = [cfd_vect cfd];
   
end


figure

plot(x,df(x))
hold on
grid on
plot(x(1:end), cfd_vect);
legend()

err_max = max(abs(df(x) - cfd_vect))





