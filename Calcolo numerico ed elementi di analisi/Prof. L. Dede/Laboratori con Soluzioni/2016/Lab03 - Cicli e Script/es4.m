%% CALCOLO NUMERICO ED ELEMENTI DI ANALISI A.A. 2013-2014 Aerospaziale
%   Prof. Antonietti Paola, Esercitatori: Brugiapaglia S., Signorini M.
%                  SOLUZIONI LABORATORIO 3 ESERCIZIO 4

close all
% scelta di N (massima numerosità del campione)
N=1000;

% faccio un ciclo sulla numerosità del campione e, per ogni n, calcolo
% l'approssimazione di pi data da PI(n).
for n=1:N
    % creazione di vettori di numeri casuali distribuiti uniformemente
    % nell'intervallo (0,1)--> la coppia di numeri casuali (x,y) conterrà 
    % dei punti che cadranno casualmente nel quadrato (0,1)x(0,1).
    x=rand(1,n);
    y=rand(1,n);

    % plot dei punti casuali ottenuti e del quarto di circonferenza
    hold off
    plot(x,y,'*')
    theta=linspace(0,pi/2,100);
    hold on
    plot(cos(theta),sin(theta),'r-');
    xlabel('x');
    ylabel('y');
    title(['n=',num2str(n)]);
    drawnow();

    % il punto (i,j) cade all'interno del primo quarto di cerchio (primo
    % quadrante) se 
    % x(i)^2+y(i)^2<1 
    % (equazione della circonferenza centrata in zero e di raggio unitario)

    m=0;

    for i=1:n
        if(x(i)^2+y(i)^2<1) 
            m=m+1;
        end
    end

    % approssimazione di pi con n :PI(n)

    PI(n)= 4*m/n;
end

% plot con le diverse approssimazioni di pi, al crescere di n, e il valore
% di pi fornito da matlab:
figure;
plot(PI);
hold on
plot([0,n],[pi,pi],'r-','LineWidth',1.5);
xlabel('n');
ylabel('\pi(n)');
title('Approssimazione di \pi');

