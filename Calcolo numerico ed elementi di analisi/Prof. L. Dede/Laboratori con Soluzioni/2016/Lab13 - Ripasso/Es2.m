addpath ../Fornite
%%%% Es 2 %%%%
close all
clear all
% definizione della matrice
n=10;
A=diag(n+1:-1:2)+diag(sin(2*pi/n*1:n-1),-1);
A(1,2:end)=1;

% definizione della soluzione esatta e calcolo del termine noto
x_ex=ones(n,1);
b=A*x_ex;

% fattorizzazione LU
[L,U,P]=lu(A);
% risoluzione del sistema
if P==eye(n)
    y=fwsub(L,b); % oppure y=L\b
    x=bksub(U,y); % oppure x=U\y
else
    % in questo caso risolvo PAx=Pb (PA=LU)
    y=fwsub(L,P*b); % oppure y=L\(P*b)
    x=bksub(U,y); % oppure x=U\y
end

err=norm(x_ex-x)/norm(x_ex);
res=norm(b-A*x)/norm(b);

fprintf(['Errore relativo : ',num2str(err),'\n']);
fprintf(['Residuo normalizzato : ',num2str(res),'\n']);

