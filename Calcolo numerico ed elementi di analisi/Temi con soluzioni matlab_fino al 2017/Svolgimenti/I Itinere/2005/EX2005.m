%% 1.1
close all
f = @(x) x.^2.*sin(x);
a=-pi/3; b=pi/2;
x = [a:0.01:b];
plot(x, f(x))
grid on

%% 1.2
tol = 1e-10;
k_min = ceil(log2((b-a)/tol)-1)

%% 1.3
[x_vect,k] = bisect(a,b,tol,f)
x_it3 = x_vect(3)
%% 1.4
x0 = 0.2;
tol = 1e-10;
itermax = 1000;
df = @(x) 2*x.*sin(x) + x.^2.*cos(x);
[x_vect,k] = newton(x0,itermax,tol,f,df)

[~,~] = stimap(x_vect)



%% 2
f = @(x) cos(x) - x;
a=-3; b=3;
x = [a:0.01:b];
x0 = -2;
tol = 1e-6;
itermax = 1000;

[x_vect, k] = quasiNChord(f,a,b,x0,tol,itermax)

x_20 = x_vect(20)
