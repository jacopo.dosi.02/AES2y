function [x_vect,k] = newton(x0,f,df,itermax,tol,m)

x = x0;
k = 0;
err = abs(f(x));
x_vect = [x0];

if nargin == 4
   tol = 1e-7;
   m = 1;
elseif nargin == 5
   m = 1;
end
    
while (err > tol) && (k < itermax)
    
    k = k+1;
    x = x - m*f(x)/df(x);
    x_vect = [x_vect; x];
    err = abs(f(x));
    
end



end

