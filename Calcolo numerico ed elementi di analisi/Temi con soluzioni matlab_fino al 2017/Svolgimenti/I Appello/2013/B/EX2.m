close all
clear all

f = @(x) cos(1./(10*x.^2 + 1 + 0.1*exp(-x)));
a = -1;
b = 1;
z = [a:0.001:b];


%% 1 EQUALLY SPACED NODES
figure
plot(z,f(z))
grid on
hold on

n_vect = [10 15];
for k = 1:length(n_vect)
   
    n = n_vect(k);
    
    x = linspace(a,b,n+1);
    p = polyfit(x,f(x),n);
    
    pz = polyval(p,z);
    
    plot(z,pz)
    
end

legend('f(x)', '\Pi_{10}f(x)', '\Pi_{15}f(x)')
title('Equally Spaced Nodes Interpolation')


%% 1 CGL NODES
figure
plot(z,f(z))
grid on
hold on

n_vect = [10 15];
for k = 1:length(n_vect)
   
    n = n_vect(k);
    
    x = [];
    for i = 0:n
       x_i = (a+b)/2 + ((b-a)/2)*(cos(i*pi/n));
       x = [x x_i];
    end
    
    p = polyfit(x,f(x),n);
    
    pz = polyval(p,z);
    
    plot(z,pz)
    
end

plot(x,0*f(x), 'ko');

legend('f(x)', '\Pi_{10}^{(CGL)}f(x)', '\Pi_{15}^{(CGL)}f(x)', '\{x_{i}^{(CGL)}\}_{i=0}^{15}')

title('CGL Nodes Interpolation')
