clear all
close all

f = @(x) exp(-x).*(3*cos(3*x)+4*sin(3*x)-cos(x));

u_ex = @(x) exp(-x).*sin(x).*cos(2*x);

N = 100;
x0 = 0;
xf = pi;
gamma = 1;
beta = 0;

h = (xf-x0)/(N+1);

knots = [x0:h:xf];

A = (1/(h^2)) * ( diag(-ones(N-1,1), -1) + diag(2*ones(N,1),0) + diag(-ones(N-1,1), 1) );
A(1,1) = 1/(h^2);

b = f(knots(2:end-1));
b(1) = b(1) - gamma/h;
b(end) = b(end) + beta/(h^2);


[~,~,u_h] = thomas( A, b );

u_0 = u_h(1) - h*gamma;
u_N = beta;
u_h = [u_0; u_h; u_N];

plot(knots, u_h);
grid on
hold on

x = [x0:0.001:xf];
plot(x, u_ex(x))

legend('u_{h}', 'u_{ex}')



h_vect = 0.1*2.^-[0:5];

e_h_vect = [];


for k = 1:length(h_vect)
    
    h = h_vect(k);
    knots = [x0:h:xf];
    
    N = length(knots) - 2;
    
    A = (1/(h^2)) * ( diag(-ones(N-1,1), -1) + diag(2*ones(N,1),0) + diag(-ones(N-1,1), 1) );
    A(1,1) = 1/(h^2);
    
    b = f(knots(2:end-1));
    b(1) = b(1) + -gamma/h;
    b(end) = b(end) + beta/(h^2);

    
    
    [~,~,u_h] = thomas( A, b );
    
    u_0 = u_h(1) - h*gamma;
    u_N = beta;
    u_h = [u_0; u_h; u_N];
    
    
    e_h = max(abs(u_ex(knots) - u_h'));
    e_h_vect = [e_h_vect e_h];
    
end


figure
loglog(h_vect, e_h_vect);
grid on
hold on
loglog(h_vect, h_vect, '--k');

legend('e_{h}', 'h^{1}')
