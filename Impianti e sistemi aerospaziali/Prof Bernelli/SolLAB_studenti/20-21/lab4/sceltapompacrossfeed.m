clc
clear
close all

%% DATI
Q_min = 1500; %[kg/h]  massimca
Q_max = 3500; %[kg/h]  massica
ro = 780; %[kg/m^3]

q = (Q_min:500:Q_max)/(ro*3600); %[m^3/s] RANGE PORTATE POMPA
p_min = 0.05 * 10^6; %[Pa]
p_max = 0.5 * 10^6; %[Pa]
L = 12; %[m]
D = 0.01905; %[m]
lambda = 0.03;
k_conc = 8;

%POMPE           pressioni [MPa]
%  Q[kg/h]  _P1     P2    P3     P4     P5
P = [1000 0.3648 0.3944 0.4338 0.4634 0.4930
     1500 0.3676 0.3974 0.4371 0.4669 0.4968
     2000 0.3678 0.3976 0.4374 0.4672 0.4970
     2500 0.3654 0.3950 0.4345 0.4641 0.4938
     3000 0.3604 0.3896 0.4286 0.4578 0.4870
     3500 0.3528 0.3814 0.4195 0.4481 0.4768
     4000 0.3426 0.3704 0.4074 0.4352 0.4630
     4500 0.3299 0.3566 0.3923 0.4190 0.4458
     5000 0.3145 0.3400 0.3740 0.3995 0.4250
     5500 0.2966 0.3206 0.3527 0.3767 0.4008
     6000 0.2760 0.2984 0.3282 0.3506 0.3730
     6500 0.2529 0.2734 0.3007 0.3212 0.3418
     7000 0.2272 0.2456 0.2702 0.2886 0.3070
     7500 0.1989 0.2150 0.2365 0.2526 0.2688
     8000 0.1680 0.1816 0.1998 0.2134 0.2270
     8500 0.1345 0.1454 0.1599 0.1708 0.1818
     9000 0.0984 0.1064 0.1170 0.1250 0.1330
     9500 0.0598 0.0646 0.0711 0.0759 0.0808
    10000 0.0185 0.0200 0.0220 0.0235 0.0250];
%% CONDIZIONI NOMINALI
k_dist = lambda*L/D;

K_dist = K(k_dist,ro,D); %help K
K_conc = K(k_conc,ro,D);                  
K_eq = K_dist + K_conc;              % Q-> ----| |----| |----

% P(:,1) sono le portate in kg/h
dp = K_eq * (q).^2; %[Pa] perdite di carico

p_pompa_min = (p_min + dp)/10^6; %[MPa]
p_pompa_max = (p_max + dp)/10^6; %[MPa]

%per plottare devo rendere l'intervallo di portate consono ai dati
q_plot = q * ro * 3600; %[kg/h]


figure(1)
% plot pompe
plot(P(:,1),P(:,2),'-r',P(:,1),P(:,3),'-b',P(:,1),P(:,4),'-g',P(:,1),P(:,5),'-k',P(:,1),P(:,6),'-y') 
hold on
%plot pressione pompa
plot(q_plot,p_pompa_min,q_plot,p_pompa_max,[3500 3500], [0.700614 0.250614], [1500 1500], [0.536847 0.0868474])
%legenda
title('CASO NOMINALE')
xlabel('PORTATE [kg/h]')
ylabel('PRESSIONI [Mpa]')
legend('Pompa 1','Pompa 2','Pompa 3','Pompa 4','Pompa 5','Pressione minima alla pompa','Pressione massima alla pompa','Qmax = 3500','Qmin = 1500')
hold off

%COMMENTO: tutte le pompe vanno bene per il caso nominale (stanno dentro
%l'area di interesse)

%% CROSS-FEED
K_eq_cf = K_eq / 4;
dp_cf = K_eq_cf * (2*q).^2; %[Pa] %devo fornire il doppie delle portate. Infatti il dp resta lo stesso, viene solo traslata l'area 
% di funzionamento sul grafico
                        
p_pompa_min_cf = (p_min + dp_cf)/10^6; %[MPa]
p_pompa_max_cf = (p_max + dp_cf)/10^6; %[MPa]

q_plot_cf = 2 * q_plot; %[kg/h]


%plot
figure(2)
% plot pompe
plot(P(:,1),P(:,2),'-r',P(:,1),P(:,3),'-b',P(:,1),P(:,4),'-g',P(:,1),P(:,5),'-k',P(:,1),P(:,6),'-y') 
hold on
%plot pressione pompa
plot(q_plot_cf,p_pompa_min_cf,q_plot_cf,p_pompa_max_cf,[7000 7000], [0.700614 0.250614], [3000 3000], [0.536847 0.0868474])
%legenda
title('CASO CROSS-FEED')
xlabel('PORTATE [kg/h]')
ylabel('PRESSIONI [Mpa]')
legend('Pompa 1','Pompa 2','Pompa 3','Pompa 4','Pompa 5','Pressione minima alla pompa','Pressione massima alla pompa','Qmax = 7000','Qmin = 3000')
hold off

fprintf('Per garantire le portate e le pressioni in Cross-Feed vanno bene solo le Pompe: 3, 4, 5');
