clear all
close all
% ****** APPELLI ******
%% ROCCO 2009
%% EX. 3
s = tf('s');
L1 = 300/(s*(s+30));
figure
bode(L1)
grid on
hold on

%% EX. 4
z = tf('z')
V = (2*z^3)/(2*z^3+z-1);
figure
impulse(V)

%% ROCCO 2010
%% EX. 1
alpha = 30
A = [-1 1 0; 0 -2 alpha; 1 0 -3];
B = [1 0 0]';
C = [1 0 0];
D = 0;
sys = ss(A,B,C,D);
K_R = ctrb(sys)
det(K_R)
K_O = obsv(sys)'
det(K_O)
%% EX. 2
s = tf('s');
G = 10/(1+0.1*s)^2;
figure
step(G)
%% EX. 4
A = [.5 0 0; 0 2 0; 1 2 0];
B = [2 0 0]';
C = [0 0 1];
D = 0;
sys = ss(A,B,C,D,-1);
figure
step(sys)
grid on
hold on
z = tf('z');
G = 2/(z*(z-.5));
step(G,'ro')


%% ROCCO 2012
%% EX. 3
s = tf('s');
L = (s-1)/((s+2)*(s-2)^2);
figure
subplot(1,2,1)
rlocus(L)
subplot(1,2,2)
rlocus(-L)
%% EX. 4
z = tf('z')
G = (z-2)/(2*z^2+0.1*z-0.1);
figure
step(G)

%% ROCCO 2014
%% EX. 1
A = [0 0 1; -1 2 0; 1 0 0];
B = [1 1 0]';
C = [1 0 0];
D = 0;

sys = ss(A,B,C,D);
K_R = ctrb(sys)
detK_R = det(K_R)
K_O = obsv(sys)
detK_O = det(K_O)