clear all
close all

A = [-3 1 0;
     0 -2 0;
     0  0 -1];
 
B = [0 -1 1]';

C = [0 0 1];

D = 0;

sys = ss(A,B,C,D);

lambdas = eig(sys)


%% Risposta Libera con stato iniziale
x0 = [0 0 1]';
[y,k,x] = initial(sys, x0)
subplot(1,2,1)
plot(x,k)
title('x(t)')
grid on
subplot(1,2,2)
plot(y,k)
title('y(t)')
grid on



[T,lambdas] = eig(A);

expA = T*diag(diag(exp(1).^lambdas))*inv(T);



%% ROCCO 2013
close all
% EX. 1
A = [1 -5; 0 -3];
B = [1 2]';
C = [0 1];
D = 0;

sys = ss(A,B,C,D);

detK_r = det(ctrb(sys))
detK_o = det(obsv(sys))

% EX. 2
s = tf('s');
G = (s-10)/((s+1)*(s+10)*(s+100));
k = [0:.01:100];
u = 100 + sin(k);
y = lsim(G,u,k);
figure
plot(k,y)
hold on
f = @(t) - 1 + 0.00707*sin(t-4.14);
plot(k,f(k))
grid on


%% ROCCO 2014
close all
% EX. 2.1
figure
subplot(1,3,1)
s = tf('s');
G = 100*s^2/(1+s)^4;
bode(G)
grid on

% EX. 2.3
subplot(1,3,2)
tu = [0:0.01:100];
u = 2 + sin(tu);
[y,k,x] = lsim(G,u,tu);
plot(k,y)
grid on

hold on
f = @(t) 25*sin(t);
plot(tu,f(tu))

% EX. 2.3
subplot(1,3,3)
step(G)
grid on

% EX. 4.4
figure
G = @(alpha) ((alpha+1)*s + alpha)/(s^3 + 5*s^2 + (5+alpha)*s + alpha);
step(G(0.01))
grid on
